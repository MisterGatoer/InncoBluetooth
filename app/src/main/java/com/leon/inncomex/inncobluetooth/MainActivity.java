package com.leon.inncomex.inncobluetooth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by leon on 2/02/16.
 */
public class MainActivity extends Activity {

    Address a;
    BluetoothSocket btSocket = null;
    ImageButton up,down,rigth,left,stop,upr,upl,downr,downl;
    ImageButton sl,eo,ml;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

   @Override
    public void onCreate(Bundle savedInstanceState) {
       setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
       super.onCreate(savedInstanceState);
       Intent newint = getIntent();
       a = (Address) newint.getSerializableExtra("Class"); //receive the address of the bluetooth device
       setContentView(R.layout.activity_main);
       new ConnectBT().execute(); //Call the class to connect

       up = (ImageButton) findViewById(R.id.arriba);
       down = (ImageButton) findViewById(R.id.abajo);
       rigth = (ImageButton) findViewById(R.id.derecha);
       left = (ImageButton) findViewById(R.id.izquierda);
       stop= (ImageButton) findViewById(R.id.alto);
       upr = (ImageButton) findViewById(R.id.arriba_derecha);
       upl = (ImageButton) findViewById(R.id.arriba_izquierda);
       downr = (ImageButton) findViewById(R.id.abajo_derecha);
       downl = (ImageButton) findViewById(R.id.abajo_izquierda);
       sl = (ImageButton) findViewById(R.id.seguidor_linea);
       eo = (ImageButton) findViewById(R.id.esquiva_obstaculo);
       ml = (ImageButton) findViewById(R.id.modo_libre);
       sl.setBackgroundResource(R.drawable.seguidorlinea2);
       eo.setBackgroundResource(R.drawable.esquivar2);

       up.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("8");
                   return true;
               }
               return false;
           }

       });
       down.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("2");
                   return true;
               }
               return false;
           }
       });
       rigth.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("6");
                   return true;
               }
               return false;
           }
       });
       left.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("4");
                   return true;
               }
               return false;
           }
       });
       stop.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("5");
                   return true;
               }
               return false;
           }
       });
       upr.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("9");
                   return true;
               }
               return false;
           }

       });
       upl.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("7");
                   return true;
               }
               return false;
           }

       });
       downr.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("3");
                   return true;
               }
               return false;
           }
       });
       downl.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {

               if(event.getAction() == MotionEvent.ACTION_UP){
                   escribir("5");
                   return true;
               }
               if(event.getAction() == MotionEvent.ACTION_DOWN){
                   escribir("1");
                   return true;
               }
               return false;
           }
       });
       sl.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               escribir("A");
               botones(false);
               sl.setBackgroundResource(R.drawable.seguidorlinea);
               eo.setBackgroundResource(R.drawable.esquivar2);
               ml.setBackgroundResource(R.drawable.control2);
               return false;
           }
       });
       eo.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               escribir("D");
               botones(false);
               sl.setBackgroundResource(R.drawable.seguidorlinea2);
               eo.setBackgroundResource(R.drawable.esquivar);
               ml.setBackgroundResource(R.drawable.control2);
               return false;
           }
       });
       ml.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               escribir("5");
               botones(true);
               sl.setBackgroundResource(R.drawable.seguidorlinea2);
               eo.setBackgroundResource(R.drawable.esquivar2);
               ml.setBackgroundResource(R.drawable.control);
               return false;
           }
       });
    }
    private void botones(boolean activado){
        up.setEnabled(activado);
        down.setEnabled(activado);
        rigth.setEnabled(activado);
        left.setEnabled(activado);
        stop.setEnabled(activado);
        upr.setEnabled(activado);
        upl.setEnabled(activado);
        downr.setEnabled(activado);
        downl.setEnabled(activado);
        if(activado){
            up.setBackgroundResource(R.drawable.arriba);
            down.setBackgroundResource(R.drawable.abajo);
            rigth.setBackgroundResource(R.drawable.derecha);
            left.setBackgroundResource(R.drawable.izquierda);
            upr.setBackgroundResource(R.drawable.derechaup);
            upl.setBackgroundResource(R.drawable.izquierdaup);
            downr.setBackgroundResource(R.drawable.derechadown);
            downl.setBackgroundResource(R.drawable.izquierdadown);
        }
        else {
            up.setBackgroundResource(R.drawable.arriba2);
            down.setBackgroundResource(R.drawable.abajo2);
            rigth.setBackgroundResource(R.drawable.derecha2);
            left.setBackgroundResource(R.drawable.izquierda2);
            upr.setBackgroundResource(R.drawable.derechaup2);
            upl.setBackgroundResource(R.drawable.izquierdaup2);
            downr.setBackgroundResource(R.drawable.derechadown2);
            downl.setBackgroundResource(R.drawable.izquierdadown2);
        }
    }
    private void Disconnect()
    {
        if (btSocket!=null) //If the btSocket is busy
        {
            try
            {
                btSocket.close(); //close connection
            }
            catch (IOException e)
            { msg("Error");}
        }
        finish(); //return to the first layout

    }
    private void escribir(String comando){
        if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write(comando.toString().getBytes());
            }
            catch (IOException e)
            {
                msg("Error");
            }
        }
    }
    // fast way to call Toast
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }
    private class ConnectBT extends AsyncTask<Void, Void, Void>{
        private boolean ConnectSuccess = true; //if it's here, it's almost connected
        private ProgressDialog progress;BluetoothAdapter myBluetooth = null;
        private boolean isBtConnected = false;
        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(MainActivity.this, "Conectando...", "Espera un momento!!!");  //show a progress dialog
        }
        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(a.getAddress());//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Fallo la Conexion.");
                finish();
            }
            else
            {
                msg("Conectado.");
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Disconnect();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onPause() {
        super.onPause();
        Disconnect();
    }
}