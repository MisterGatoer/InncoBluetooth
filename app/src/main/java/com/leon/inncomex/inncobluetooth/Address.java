package com.leon.inncomex.inncobluetooth;

import java.io.Serializable;

/**
 * Created by leon on 2/02/16.
 */
public class Address implements Serializable {
    String Address;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
