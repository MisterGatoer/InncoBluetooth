#include <Servo.h>
#include <InnShield.h>
#include <Ultrasonic.h>

InnShield motor1 (M1);
InnShield motor2 (M2);
InnShield sensor1 (SL1);
InnShield sensor2 (SL2);
Ultrasonic ultrasonic(11, 12);

const int velM = 100;    // Velocidad de los motores (0-100)
const int velm = 50;
int estados =5;
void setup()  { 
  Serial.begin(9600);   // inicia el puerto serial para comunicacion con el Bluetooth
} 

void loop()  { 

  if(Serial.available()>0){
    estados = Serial.read();
    estados = estados-48;
  }
  Serial.println(estados);
  if(estados== 17){
    seguidor();
  }
  if(estados== 20){
    esquiva();
  }
  for (int i = 1; i < 10; i++){
    if(estados == i ){
      libre(estados);
    }
  }
}

void esquiva(){
  if (ultrasonic.Ranging(CM) <= 12) {

    motor1.atras(75);
    motor2.atras(75);

    delay(300);

    motor1.adelante(75);
    motor2.atras(75);
    delay(300);
  }else{
    motor1.adelante(75);
    motor2.adelante(75);
  }
}

void seguidor(){
  sensor1.lectura();// la funcion "lectura", obtendra los datos que detecta el sensor asignado en este caso lectura guardara los datos en "Sensor1"
  sensor2.lectura();

  if(sensor1.lectura()==1){// si el sensor 1 deteca el color negro le dira al robot que regrese a la linea activando el motor 1
    motor1.adelante(75);
    motor2.parar();
  }
  if(sensor2.lectura()==1){// si el sensor 2 deteca el color negro le dira al robot que regrese a la linea activando el motor 2
    motor1.parar();
    motor2.adelante(75);
  }
}

void libre(int estados){
    switch(estados){
    case 1:
      motor1.atras(velm);
      motor2.atras(velM);
      break;
    case 2:
      motor1.atras(velM);
      motor2.atras(velM); 
      break;
    case 3:
      motor1.atras(velM);
      motor2.atras(velm); 
      break;
    case 4:      
      motor2.adelante(velm);
      motor1.atras(velm);
      break;
    case 5:
      motor1.parar();
      motor2.parar();
      break;
    case 6:
      motor1.adelante(velm);
      motor2.atras(velm);
      break;
    case 7:
      motor1.adelante(velm);
      motor2.adelante(velM);
      break;
    case 8:
      motor1.adelante(velM);
      motor2.adelante(velM);
      break;
    case 9:
      motor1.adelante(velM);
      motor2.adelante(velm);
      break;      
    }
}

